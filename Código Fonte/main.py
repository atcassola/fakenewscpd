import struct
import pandas
import math
from trieBuilder import *
from quickSort import *
import time
from tkinter import *
from tkinter import ttk # para a combobox

#page_headline, body, tags, claim_label, date, page_shares, page_domain, page_url
registro = struct.Struct("500s 1000s 100s 10s 10s i 50s 300s")
tamanhoRegistro = registro.size

if os.path.exists("trieTags.bin"): #confere se o arquivo existe
    with open("trieTags.bin",'rb') as openfile:  #with automaticamente da um close() no final
        rootTags = pickle.load(openfile)

if os.path.exists("trieHosts.bin"): #confere se o arquivo existe
    with open("trieHosts.bin",'rb') as openfile:  #with automaticamente da um close() no final
        rootHosts = pickle.load(openfile)

def stripString(text):
	maxTextLength = 25
	decodedText = text.decode('utf8').rstrip(b'\x00'.decode('utf8'))
		
	if not decodedText:
		return "NULL                        "
	else:
		if len(decodedText) < maxTextLength :
			return decodedText
		else:
			return decodedText[:maxTextLength]+"..."
		

		
def recuperaRegistros(binFile, ids):
	listaRetorno = []
	for i in ids:
		binFile.seek(i*tamanhoRegistro)
		registro_binario = binFile.read(registro.size)
		registro_resgatado = registro.unpack(registro_binario)
		subdict = {}
		subdict['ID'] = str(i)
		subdict['MANCHETE'] = stripString(registro_resgatado[0])
		subdict['TEXTO'] = stripString(registro_resgatado[1])
		subdict['TAGS'] = stripString(registro_resgatado[2])
		subdict['VERACIDADE'] = stripString(registro_resgatado[3])
		subdict['DATA'] =  time.strptime(stripString(registro_resgatado[4]), "%m/%d/%Y")
		subdict['SHARES'] = registro_resgatado[5]
		subdict['HOST'] = stripString(registro_resgatado[6])
		subdict['URL'] = stripString(registro_resgatado[7])
		listaRetorno.append(subdict)
	return listaRetorno
		
def retiraFalsos(listaNoticias):
	noticiasParaRetirar = []
	for noticia in listaNoticias:
		if not (noticia['VERACIDADE'] == "TRUE"):
			noticiasParaRetirar.append(noticia)
		else:
			pass
			
	for i in range(len(noticiasParaRetirar)):
		listaNoticias.remove(noticiasParaRetirar[i])
		
def intersection2(lst1, str1, lst2, str2):
	if not str1:
		return lst2
	elif not str2:
		return lst1
	else:
		return list(set(lst1) & set(lst2))
		

def intersection3(lst1, str1, lst2, str2, lst3, str3):
	if not str1:
		return intersection2(lst2, str2, lst3, str3)
	elif not str2:
		return intersection2(lst1, str1, lst3, str3)
	elif not str3:
		return intersection2(lst2, str2, lst1, str1)
	else:
		return list(set(lst1) & set(lst2) & set(lst3))
		
def intersection4(lst1, str1, lst2, str2, lst3, str3, lst4, str4): 
	if not str1:
		return intersection3(lst2, str2, lst3, str3, lst4, str4)
	elif not str2:
		return intersection3(lst1, str1, lst3, str3, lst4, str4)
	elif not str3:
		return intersection3(lst2, str2, lst1, str1, lst4, str4)
	elif not str4:
		return intersection3(lst2, str2, lst3, str3, lst1, str1)
	else:
		return list(set(lst1) & set(lst2) & set(lst3) & set(lst4))
		
def realizaPesquisa(tag1, tag2, tag3, host, ordenacao, apenasTrue):
	resultadoPesquisa = []
	
	IDsComTag1 = findTag(rootTags, tag1)
	IDsComTag2 = findTag(rootTags, tag2)
	IDsComTag3 = findTag(rootTags, tag3)
	IDsComHost = findTag(rootHosts, host)
	
	Intersecao = intersection4(IDsComTag1, tag1, IDsComTag2, tag2, IDsComTag3, tag3, IDsComHost, host)
	
		
	if not Intersecao:
		output.config(state=NORMAL)   # habilita edição da text box
		output.delete(0.0, END)   # apaga a label de saída antes de printar novos resultados
		output.insert(END, "Sua pesquisa não retornou resultados")   # insere resultado
		output.config(state=DISABLED) # desabilita edição da text box novamente
		return
	else:		
		with open("registroNoticias.bin", "rb") as arquivo:
			resultadoPesquisa = recuperaRegistros(arquivo, Intersecao)
	
	if apenasTrue:
		retiraFalsos(resultadoPesquisa)
	
	if not resultadoPesquisa:
		output.config(state=NORMAL)   # habilita edição da text box
		output.delete(0.0, END)   # apaga a label de saída antes de printar novos resultados
		output.insert(END, "Sua pesquisa não retornou resultados")   # insere resultado
		output.config(state=DISABLED) # desabilita edição da text box novamente
	else:
		stringSaida = ""
		
		quicksort(resultadoPesquisa, 0, len(resultadoPesquisa)-1, ordenacao)
		stringSaida = stringSaida+"|MANCHETE                   |URL                         |DATA      |SHARES  |VERACIDADE\n"
		for noticia in resultadoPesquisa:
			stringShares = ""
			intShares = noticia["SHARES"]
			stringShares = str(intShares)
			stringManchete = noticia["MANCHETE"]
			
			while len(stringShares) < 6:
				stringShares = " "+stringShares
			
			while len(stringManchete) < 28:
				stringManchete = stringManchete+" "
			
				
			stringSaida = stringSaida+"%s|%s|%s|  %s|%s\n"%(stringManchete, noticia["URL"], time.strftime("%d/%m/%Y", noticia["DATA"]),stringShares, noticia["VERACIDADE"])
    
		output.config(state=NORMAL)   # habilita edição da text box
		output.delete(0.0, END)   # apaga a label de saída antes de printar novos resultados
		output.insert(END, stringSaida)   # insere resultado
		output.config(state=DISABLED) # desabilita edição da text box novamente
		



'''

    CONSTRUÇÃO DA GUI

'''


window = Tk() # cria janela

window.title("Peganamentira - Fake News")
window.geometry("800x650")
window.configure(background="white")
window.iconbitmap("images/fn_icon_final.ico")
window.resizable(width=False, height=False) # bloqueia redimensionamento <<<<<


# cover Fake news
cover = PhotoImage(file="images/cover2.gif")
Label (window, image=cover, bg="white") .grid(row=0, column=0, columnspan=4, sticky=EW)


# Label indicando que os inputs são para tags
Label (window, text="TAGs:", bg="white", fg="black", font="Arial 12 bold").grid(row=1, column=0, sticky=W, pady=(10,0), padx=(5,0))

# inputs de tags:
tag1 = Entry(window, font="none 11", bg="white")
tag1.grid(row=1, column=1, sticky=E, pady=(10,0))

tag2 = Entry(window, font="none 11", bg="white")
tag2.grid(row=1, column=2, sticky=E, padx=4, pady=(10,0))

tag3 = Entry(window, font="none 11", bg="white")
tag3.grid(row=1, column=3, sticky=E, pady=(10,0))


# Label indicando que o input é para host
Label (window, text="Host:", bg="white", fg="black", font="Arial 12 bold").grid(row=2, column=0, sticky=W, pady=(5,0), padx=(5,0))

# input de host
hostIn = Entry(window, font="none 11", bg="white")
hostIn.grid(row=2, column=1, sticky=E, pady=(5,0))


# combobox seletor de DATA/SHARES
comboboxDS = ttk.Combobox(window, state="readonly")
comboboxDS.grid(row=2, column=2, sticky=W, pady=(5,0), padx=(15,0))
# valores do combobox
comboboxDS['values']= (
    "DATA",
    "SHARES"
)
# valor default
comboboxDS.current(newindex=0)


# botão de check para habilitar/desabilitar apenas notícias verdadeiras
varCheckButton = IntVar()
checkButton = Checkbutton(window, text="Apenas notícias verdadeiras", variable=varCheckButton, bg="white")
checkButton.grid(row=2, column=3, columnspan=2, sticky=W, pady=(5,0))


msgInicial = "Utilize os campos acima para configurar sua pesquisa no banco de dados de notícias. Clique no botão lá embaixo para executar a pesquisa com seus parâmetros :D"

# cria textbox de saída
output = Text (window, width=95, height=19, wrap=WORD)
output.grid(row=3, column=0, columnspan=4, padx=4, pady=(15,7), sticky=W)
output.insert(END, msgInicial)
output.config(state=DISABLED)   # não permite mexer na text box (tem que habilitar para inserir texto mesmo com .insert())
# scrollbar  para textbox:
scrollb = Scrollbar(window, command=output.yview)
scrollb.grid(row=3, column=0, columnspan=4, padx=(775,0), pady=(15,7), ipady=128)   # sendo ipady o tamanho da scrollbar
output['yscrollcommand'] = scrollb.set


# botão
botao = Button(window, text="Buscar", width=5, command= lambda: realizaPesquisa(tag1.get(), tag2.get(), tag3.get(), hostIn.get(), comboboxDS.get(), varCheckButton.get()))
botao.grid(row=4, column=0, columnspan=7, padx=(500,0), sticky=E)


window.mainloop()






#FUNÇÃO DE PRÉ-PROCESSAMENTO DO ARQUIVO registroNoticias.bin ======================================================

'''
#						page_headline, body, tags, claim_label, date, page_shares, page_domain, page_url
registro = struct.Struct("500s 1000s 100s 10s 10s i 50s 300s")
tamanhoRegistro = registro.size
csvDataFrame = pandas.read_csv('emergent.csv')
with open("registroNoticias.bin", "wb") as binFile:
	for i in range(len(csvDataFrame.index)):
		
		manchete = csvDataFrame['page_headline'][i]
		if not(isinstance(manchete, str)):
			manchete = ""
		
		corpo = csvDataFrame['body'][i]
		if not(isinstance(corpo, str)):
			corpo = ""
			
		tags = csvDataFrame['tags'][i]
		if not(isinstance(tags, str)):
			tags = ""
		tags.replace("+", " ")
		
		veracidade = csvDataFrame['claim_label'][i]
		if not(isinstance(veracidade, str)):
			veracidade = ""	
		
		data = csvDataFrame['date'][i]
		if not(isinstance(data, str)):
			data = ""
			
		shares = int(csvDataFrame['page_shares'][i].replace(',',''))
		if not(isinstance(shares, int)):
			shares = 0

		host = csvDataFrame['page_domain'][i]
		if not(isinstance(host, str)):
			host = ""
			
		url = csvDataFrame['page_url'][i]
		if not(isinstance(url, str)):
			url = ""
		
		registro_binario = registro.pack(manchete.encode('utf8'), corpo.encode('utf8'), tags.encode('utf8'), veracidade.encode('utf8'), data.encode('utf8'), shares, host.encode('utf8'), url.encode('utf8'))
		binFile.write(registro_binario)
'''

#=========================================================================================================================