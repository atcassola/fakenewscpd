def particao(lista, inicio, fim, parametro):
#escolhe como pivô sempre o último item; 
#bottom e top são os ponteiros que percorrem a particao do início até o fim e do fim até o início, respectivamente;
#vai fazendo os swaps até bottom == top (ou seja, até os ponteiros se encontrarem, o que significa que a partição inteira foi analisada)
	pivo = lista[fim]
	bottom = inicio-1
	top = fim

	done = 0
	while not done:

		while not done:
			bottom = bottom + 1

			if bottom == top:
				done = 1
				break

			if lista[bottom][parametro] > pivo[parametro]:
				lista[top] = lista[bottom]
				break

		while not done:
			top = top-1

			if top == bottom:
				done = 1
				break

			if lista[top][parametro] < pivo[parametro]:
				lista[bottom] = lista[top]
				break

	lista[top] = pivo
	return top

def quicksort(lista, inicio, fim, parametro):	
 #algoritmo padrão do quicksort, particiona recursivamente a lista e vai ordenando de acordo com o parametro de ordenação
 #assume que a lista é uma lista de dicionários e que todos os dicionários tem no mínimo a chave str(parametro) e que esta tem um valor numérico
	if inicio < fim:
		split = particao(lista, inicio, fim, parametro)
		quicksort(lista, inicio, split-1, parametro)
		quicksort(lista, split+1, fim, parametro)
	else:
		return