from unicodedata import normalize
import pandas as pd
import pickle
import os

# Função para formatação da tag buscada desconsiderando maiúsculas e minúsculas e que remove acentos
def formatTag(tag):
    tag = normalize('NFKD', tag).encode('ASCII', 'ignore').decode('ASCII') # retira os acentos
    return tag.lower() # devolve a tag toda em minúscula



"""

    Definição da TRIE e suas funções de adição e busca

"""

class Trie(object):

    def __init__(self, char: str):
        self.char = char   # caractere do nodo atual
        self.sons = []   # nodos filhos
        self.endTag = False   # se existe uma tag finalizada ali
        self.id = []   # lista vazia para nodos que não são término de palavra
        # não é um índice único e sim uma lista, pois podem existir várias notícias com as mesmas tags

def addTag(root, tag, ID):   # função usada para adicionar uma nova tag à estrutura trie

    tag = formatTag(tag)   # formata string de entrada para o padrão (todas as letras minúsculas sem acento)
    node = root   # começa a iteração pela raiz
    for char in tag:   # itera pelos caracteres da tag tentando encontrar o nodo com aquele caractere
        found = False   # indica se o caractere atual já existe na lista de nodos filhos

        for son in node.sons:  # busca pelo caractere nos filhos do nodo atual
            if son.char == char:
                node = son   # apontamos o nodo para o filho que contém esse char
                found = True   # indicamos que o char foi encontrado nos filhos e não precisa ser adicionado
                break

        if not found:   # se o char não foi encontrado, adiciona novo filho
            newNode = Trie(char)
            node.sons.append(newNode)
            node = newNode   # apontamos, então, o nodo para seu novo filho e continuamos a iteração
                            # note que a partir daqui a iteração vai ser sempre nula pois o novo nodo ainda não tem filhos

    node.endTag = True   # indica que até ali pode ser uma tag
    node.id.append(ID)   # assinala o id passado



def findTag(root, tag):   # função usada na busca de tags na trie, retorna [] se não encontrar, ou uma lista com os ids

    tag = formatTag(tag)   # formata string de entrada para o padrão (todas as letras maiúsculas sem acento)
    node = root   # começa a iteração pela raiz

    if not root.sons:   # se o nodo raiz não tiver nenhum filho, trivial, retorna falso (lista vazia)
        return []
    for char in tag:
        notFound = True   # padrão indicando que o caractere não foi encontrado
        for son in node.sons:
            if son.char == char:
                notFound = False   # assinala que o char foi encontrado
                node = son   # passa iteração para o nodo filho
                break

        if notFound:   # indica que não encontrou a tag retornando lista vazia
            return []

    # Caso passe por todos os caracteres sem retornar lista vazia, então a tag foi encontrada
    # checa se aquele nodo é um nodo final com ids
    if node.endTag:
        return node.id
    else:
        return []	

		
		
		
		
		
# ROTINAS DE PRÉ-PROCESSAMENTO		
		
		
'''		
"""

    Leitura do dataframe e criação da TRIE de hosts

"""


df = pd.read_csv('emergent.csv')

hostsTrie = Trie('#')   # cria raiz da trie de hosts
index = 0   # cada linha corresponde a um index+1
for host in df['page_domain']:   # itera pelos hosts do dataframe
	if type(host) == float:   # se não tem nenhum host correspondendo à notícia, adiciona a noHost
		addTag(hostsTrie, "noHost", index)
	else:
		addTag(hostsTrie, host, index)# adiciona o host desta linha apontando para aquele indice
	index += 1 # aumenta o valor de index porque será analisada a próxima linha

# salvando a trie em arquivo binário
with (open("trieHosts.bin", 'wb+')) as openfile:
	pickle.dump(hostsTrie, openfile)		
		
'''
		
		
		
		
'''

"""

    Leitura do dataframe e criação da TRIE de tags

"""


df = pd.read_csv('emergent.csv')

tagsTrie = Trie('#')   # cria raiz da trie de tags
index = 0   # cada linha corresponde a um index+1
for tag in df['tags']:   # itera pelas tags do dataframe
    if type(tag) == float:   # se não tem nenhuma tag correspondendo à notícia, adiciona à untagged
        addTag(tagsTrie, "untagged", index)
    else:
        listTags = tag.replace('+',' ').split(',')  # troca os + por espaço e separa tudo por vírgula
        for item in listTags:
            addTag(tagsTrie, item, index)   # adiciona cada tag da lista apontando para aquele indice

    index += 1   # aumenta o valor de index porque será analisada a próxima linha


# salvando a trie em arquivo binário
with (open("trieTags.bin", 'wb+')) as openfile:
    pickle.dump(tagsTrie, openfile)

'''


"""
    Para abrir o arquivo, basta:

if os.path.exists("trieTags.bin"): #confere se o arquivo existe
    with open("trieTags.bin",'rb') as openfile:  #with automaticamente da um close() no final
        teste = pickle.load(openfile)

    Exemplo de busca:
findTag(teste, "australia")

    Deve retornar:
[3, 4, 5, 6, 7, 8, 9, 10, 11, 1713, 1714]

"""
